#---activate the env:
.\env\Scripts\activate
#--- to start Django service -> run server:
cd image_project
python manage.py runserver


#endpoints
http://127.0.0.1:8000/add_image/ ----- add image with unique id, unique name
http://127.0.0.1:8000/images/ ------ listview for display images
http://127.0.0.1:8000/get_image/ ---- a view for download img with passed parammeters, PIL logic located inside images/image_func/transform_image(path, right, bottom), haven't finishing tested it
http://127.0.0.1:8000/ --- some base GET,POST,PUT methods


models of Image:
 id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)  # creating an autounique key for imgs
 title = models.TextField() -- specyfic name 
 cover = ImageField(upload_to='images/') ----- all data located insite media/images folder
    