from django.urls import path

from .views import HomePageView,LoadImageView, ImageView, GetImageView

urlpatterns = [
    path('images/', HomePageView.as_view(), name='home'),
    path('add_image/', LoadImageView.as_view(), name='add_image'),
    path('', ImageView.as_view(), name = 'images'),
    path('/<str:title>', ImageView.as_view()),
    path('get_image/', GetImageView.as_view(), name = 'get_images'),

]
