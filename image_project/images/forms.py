from django import forms
from .models import Image

class ImageLoadForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'cover']

class GetImageForm(forms.Form):
    title = forms.ModelChoiceField(queryset=Image.objects.all(), required=True)
    right = forms.IntegerField(required=True)
    bottom = forms.IntegerField(required=True)