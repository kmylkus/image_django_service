from django.shortcuts import render
from django.views.generic import ListView, CreateView ,TemplateView
from django.urls import reverse_lazy 
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from http import HTTPStatus as status
from wsgiref.util import FileWrapper
from django.http import HttpResponse
import mimetypes
import os
from .models import Image
from .forms import ImageLoadForm, GetImageForm
from .serializers import ImageSerializer
from .image_func import transform_image
    
class HomePageView(ListView):
    model = Image
    template_name = 'home.html'

#--- adding_image
class LoadImageView(CreateView): 
    model = Image
    form_class = ImageLoadForm
    template_name = 'add_image.html'
    success_url = reverse_lazy('home')

#--- getting data from models
class ImageView(APIView):
    def get(self, request, *args, **kwargs):
        images = Image.objects.all()
        serializer = ImageSerializer(images, many=True)
        return Response({"images": serializer.data})

    def post(self, request):
        image = request.data.get('image')
        # Create an article from the above data
        serializer = ImageSerializer(data=image)
        if serializer.is_valid(raise_exception=True):
            images_saved = serializer.save()
        return Response({"success": "Image '{}' added successfully".format(images_saved.title)})

    def put(self, request, title):
        saved_image = get_object_or_404(Image.objects.all(), title=title)
        data = request.data.get('title')
        serializer = ImageSerializer(instance=saved_image, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            image_saved = serializer.save()
        return Response({
            "success": "Image '{}' updated successfully".format(image_saved.title)
        })

#--downloading_image
class GetImageView(TemplateView):
    template_name = 'select_image.html'

    def get(self, request):
        form = GetImageForm()
        images = Image.objects.all()
        args = {'form': form}
        return render(request, 'select_image.html', args)

    def post(self, request):
        form = GetImageForm(request.POST)
        if form.is_valid():
            title = request.POST.get('title')
            right = request.POST.get('rigth')
            bottom = request.POST.get('bottom')
            img = Image.objects.filter(id=title)
            for f in img:
                file_path =f.cover
                file_name = f.title
            #transform_image(file_path.url,right,bottom) --- manipulation with image data
            args = { 'objects': img}
            return render(request, 'image.html', args)
        args = {'form': form, 'object': file_path}
        return render(request, 'select_image.html', args)


        

