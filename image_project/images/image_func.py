from PIL import Image

def transform_image(file_path,right,bottom):
	img = Image.open(file_path)
	x,y = img.size
	img = img.resize((x//2,y//2),Image.ANTIALIAS)
	width, height = img.size   # Get dimensions
	# left = width/4
	# top = height/4
	# right = 3 * width/4
	# bottom = 3 * height/4
	cropped_example = img.crop((0, 0, right, bottom))
	return cropped_example


