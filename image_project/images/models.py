from django.db import models
import uuid
# Create your models here.

class Image(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)  # using the function uuid4 on the module
    title = models.TextField()
    cover = models.ImageField(upload_to='images/')
    
    def __str__(self):
        return self.title