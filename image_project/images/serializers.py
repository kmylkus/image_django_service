
from rest_framework import serializers

class ImageSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    title = serializers.CharField(max_length=120)
    cover = serializers.ImageField()

