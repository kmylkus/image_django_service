# Generated by Django 2.1.5 on 2019-10-28 14:05

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0003_auto_20191028_1603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='id',
            field=models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False),
        ),
    ]
